#include <math.h>
#include <stdio.h>

#include "/usr/include/python2.7/Python.h"

int calls;

double Function( double x )
{
  calls++;
  //return -sin(0.5*x);
  return 0.5*x*x + 4*sin(x);   
}

double A = -2, B = 0;//3.1415 * 3;

double epsilon = 1e-3;

void ExtremumLoc(double &a, double &b, double eps)
{
  int i, n = 5;
  double x, delta = (b - a) / n, f_x, f_prev;

  f_prev = Function(a + delta);
  for (i = 2; i <= n; i++)
  {
    x = a + delta * i;
    f_x = Function(x);
    if (f_x > f_prev)
      break;
    f_prev = f_x;
  }
  a = x - 2 * delta;
  b = x;
}

void Dichotomy(double &a, double &b, double _eps)
{
  double middle = (a + b) / 2, eps = (b - a) / 20;

  if (Function(middle - eps) < Function(middle + eps))
    b = middle;
  else
    a = middle;
}
  
void GoldenSection(double &a, double &b, double eps)
{
  double alpha = (sqrt(5.f) - 1.f) / 2.f;
  static double lambda, mu, f_lambda, f_mu;

  if (calls == 0)
  {
    lambda = a + (1 - alpha) * (b - a);
    mu = a + b - lambda;
    f_lambda = Function(lambda);
    f_mu = Function(mu);
  }

  if (f_lambda < f_mu)
  {
    b = mu;
    if (fabs(b - a) < eps)
      return;
    mu = lambda;
    f_mu = f_lambda;
    lambda = a + (1 - alpha) * (b - a);
    f_lambda = Function(lambda);
  }
  else
  {
    a = lambda;
    if (fabs(b - a) < eps)
      return;
    lambda = mu;
    f_lambda = f_mu;
    mu = a + alpha * (b - a);
    f_mu = Function(mu);
  }
}

void Test(char *name, void (*Iterate)(double &, double &, double),
          double a, double b)
{
  int step = 0;

  calls = 0;
  printf("\nRun %s\n", name);
  while (fabs(a - b) > epsilon)
  {
    Iterate(a, b, epsilon);
    step++;
    printf("step %2d: (%5.3lf, %5.3lf) (%5.5lf, %5.5lf)\n", step, a, b,
		(a + b)/2, Function((a + b)/2));
        calls--;
  }
  printf("Done in %d calls to function\n", calls);
}  

int main( void )
{
    Py_Initialize();
       PyRun_SimpleString("import pylab");
       PyRun_SimpleString("pylab.plot(range(5))");
       PyRun_SimpleString("pylab.show()");
       Py_Exit(0);
       return 0;
    
  printf("a = %6.4lf, b = %6.4lf\n", A, B);
  Test("Extremum localization", ExtremumLoc, A, B);
  //Test("dichotony", Dichotomy, A, B);
  Test("Golden section search", GoldenSection, A, B);
//  scanf("%*c");
  return 0;
}
