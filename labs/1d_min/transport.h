#pragma once

#include <OptMethods>
#include <vector>
#include <stack>
#include <map>

// Vector data type representation
template<class Type>
struct Vector
{
  Vector() : n_(0) {}
  Vector(Type * data, int N)
    : n_(N)
  {
    data_.resize(N);
    for (int i = 0; i < N; i++)
      data_[i] = data[i];
  }

  typename std::vector<Type>::reference operator() (int i) { return data_[i]; }
//  Type operator() (int i) { return data_[i]; }
  Type const& operator() (int i) const { return data_[i]; }
  void set(int i, Type value) { data_[i] = value; }

  int n() const { return n_; }

  void resize(int N)
  {
    std::vector<Type> oldData = data_;
    data_.resize(N, 0);
    for (int j = 0; j < n_; j++)
      data_[j] = oldData[j];

    n_ = N;
  }

  void null()
  {
    for (int i = 0; i < n_; i++)
      data_[i] = 0;
  }

private:
  std::vector<Type> data_;
  int n_;
};

template<class Type>
void print(Vector<Type> const& m)
{
  printf("[ ");
  for (int j = 0; j < m.n(); j++)
    printf("%2.0lf  ", m(j));
  printf("]\n");
}


// 2D Matrix data type representation
template<class Type>
struct Matrix
{
  Matrix() : n_(0), m_(0)  {}

  Matrix(Type * data, int N, int M)
    : m_(M), n_(N)
  {
    data_.resize(M * N);
    for (int i = 0; i < N; i++)
      for (int j = 0; j < M; j++)
        data_[to1d(i, j)] = data[to1d(i, j)];
  }

  Type & operator() (int i, int j)
  {
    assert(i < n_ && j < m_);
    return data_[to1d(i, j)];
  }
  Type const& operator() (int i, int j) const
  {
    assert(i < n_ && j < m_);
    return data_[to1d(i, j)];
  }
  int n() const { return n_; }
  int m() const { return m_; }

  void resize(int N, int M)
  {
    int i, j;
    std::vector<Type> oldData = data_;
    data_.resize(M * N);
    for (i = 0; i < n_; i++)
      {
        for (j = 0; j < m_; j++)
          {
            if (i < n_ && j < m_)
              data_[to1d(M, i, j)] = oldData[to1d(i, j)];
            else
              data_[to1d(M, i, j)] = 0;
          }
      }

    n_ = N;
    m_ = M;
  }

  void null()
  {
    for (int i = 0; i < n_; i++)
      for (int j = 0; j < m_; j++)
        data_[to1d(i, j)] = 0;
  }


private:
  int to1d(int M, int i, int j) const { return i * M + j; }
  int to1d(int i, int j)        const { return to1d(m_, i, j); }

private:
  std::vector<Type> data_;
  int m_; // columns count
  int n_; // rows count
};

template<class Type>
void print(Matrix<Type> const& m)
{
  for (int i = 0; i < m.n(); i++)
    {
      printf("[ ");
      for (int j = 0; j < m.m(); j++)
        printf("%2.0lf  ", m(i, j));
      printf("]\n");
    }
}


template<class T>
struct TransportProblem
{
private:
  typedef ::Matrix<T>      Matrix;
  typedef ::Matrix<int>    MatrixInt;
  typedef ::Vector<T>      Vector;
  typedef ::Vector<bool>   VectorBool;

  // Cell data type representation
  struct Cell
  {
    int i, j;
    bool vertDir;
    int counter;

    Cell() {}
    Cell(int ii, int jj, bool VertDir, int Counter)
      : i(ii), j(jj), vertDir(VertDir), counter(Counter)
    {}
  };

public:
  TransportProblem(Matrix const& costs, Vector const& needs, Vector const& storage)
    : costs_(costs)
    , needs_(needs)
    , storage_(storage)
  {
    n_ = costs.n();
    m_ = costs.m();
  }

public:
  // Solve problem
  void solve()
  {
    makeClose();
    printf("Costs:\n");
    print(costs_);
    printf("Needs:\n");
    print(needs_);
    printf("Storage:\n");
    print(storage_);

    formInitialPlan();
    printf("\n");
    printf("Initial plan:\n");
    printPlanWeight();
    print(plan_);

    int i;
    for (i = 0; i < 100; i++)
      {
        calculatePotentials();
        /*printf("u: ");
        print(u_);
        printf("v: ");
        print(v_);*/

        if (!increasePlan())
          break;

        /*printf("\n");
        printf("Current plan:\n");
        printPlanWeight();
        print(plan_);
        getch();
        */
      }

    printf("\n");
    printf("Optimal plan:\n");
    printPlanWeight();
    print(plan_);

    printf("\nIterations: %d\n", i);
  }

private:
  void printPlanWeight()
  {
    double weight = 0;
    for (int i = 0; i < n_; i++)
      for (int j = 0; j < m_; j++)
        weight += plan_(i, j) * costs_(i, j);

    printf("Plan weight = %8.3lf\n", weight);
  }

  // Create closed transport problem by adding imaginary store or market
  void makeClose()
  {
    T needsSum = 0, storageSum = 0;

    // calc sums
    int i;
    for (i = 0; i < m_; i++)
      needsSum += needs_(i);
    for (i = 0; i < n_; i++)
      storageSum += storage_(i);

    if (needsSum > storageSum)
      {
        // Add empty row (fake supplier)
        n_++;
        storage_.resize(n_);
        storage_.set(n_ - 1, needsSum - storageSum);
        costs_.resize(n_, m_);
      }
    else if (storageSum > needsSum)
      {
        // Add empty column (fake consumer)
        m_++;
        needs_.resize(m_);
        needs_.set(m_ - 1, storageSum - needsSum);
        costs_.resize(n_, m_);
      }
  }

  // Form initial plan with "North-West angle" method
  void formInitialPlan()
  {
    plan_.resize(n_, m_);
    filledFlag_.resize(n_, m_);

    int i = 0, j = 0;
    T right = storage_(0);
    T down  = needs_(0);

    while (i < n_ && j < m_)
      {
        filledFlag_(i, j) = 1;

        if (right <= down)
          {
            // move down
            plan_(i, j) = right;
            down -= right;
            i++;
            right = storage_(i);
          }
        else
          {
            // move right
            plan_(i, j) = down;
            right -= down;
            j++;
            down = needs_(j);
          }
      }
  }

  void calculatePotentials()
  {
    u_.resize(n_);
    v_.resize(m_);

    // U1=0
    u_.set(0, 0);
    filledU_(0) = true;

    //Ui + Vj = Cij
    for (int i = 0; i < n_; i++)
      for (int j = 0; j < m_; j++)
        if (filledFlag_(i, j))
          {
            if (filledU_(i) && !filledV_(j))
              {
                v_.set(j, costs_(i, j) - u_(i));
                filledV_.set(j, true);
                i = 0, j = 0;
              }
            else if (!filledU_(i) && filledV_(j))
              {
                u_.set(i, -v_(j) + costs_(i, j));
                filledU_.set(i, true);
                i = 0, j = 0;
              }
          }
  }

  bool increasePlan()
  {
    const double eps = 1e-10;

    // Find cell with maximum potentials delta
    //Δij = (Ui + Vj) - Сij
    double maxD = 0;
    int maxI, maxJ;
    for (int i = 0; i < n_; i++)
      for (int j = 0; j < m_; j++)
        if (!filledFlag_(i, j))
          {
            double d = (v_(j) + u_(i)) - costs_(i, j);
            if (d > maxD)
              maxD = d, maxI = i, maxJ = j;
          }

    // Is optimal plan?
    if (fabs(maxD) < eps)
      return false;

    // Find cycle
    filledFlag_(maxI, maxJ) = true;
    int size = findLoop(maxI, maxJ);

//    for (int i = 0; i < size; i++)
//            printf("(%d, %d) -> ", cycle_[i].i, cycle_[i].j);
//    printf("\n");

    // Find cell in cycle with minimum weight
    T minWeight = plan_(cycle_[0].i, cycle_[0].j);
    int minI = cycle_[0].i, minJ = cycle_[0].j;
    int k;
    for (k = 2; k < size; k += 2)
      {
        T weight = plan_(cycle_[k].i, cycle_[k].j);
        if (weight < minWeight)
          minWeight = weight, minI = cycle_[k].i, minJ = cycle_[k].j;
      }

    // Balancing plan
    filledFlag_(minI, minJ) = false;
    for (k = 0; k < size; k++)
      if (k & 1) // 0
        plan_(cycle_[k].i, cycle_[k].j) += minWeight;
      else       // 1
        plan_(cycle_[k].i, cycle_[k].j) -= minWeight;

    return true;
  }


  // Find loop using depth search with recovering
  int findLoop(int i, int j)
  {
    std::stack<Cell> S;
    visitFlagVert_.resize(n_, m_);
    visitFlagVert_.null();
    visitFlagHor_.resize(n_, m_);
    visitFlagHor_.null();

    int counter = 0;

    pushCells(S, i, j, false, 0);
    pushCells(S, i, j, true, 0);

    while (!S.empty())
      {
        Cell cell = S.top();
        S.pop();

        counter = cell.counter;
        cycle_[counter] = cell;

        if (cell.i == i && cell.j == j)
          break;

        pushCells(S, cell.i, cell.j, !cell.vertDir, counter + 1);
      }

    return counter + 1;
  }

  // Push cells in some direction from current
  void pushCells(std::stack<Cell>& S, int i, int j, bool vertDir, int counter)
  {
    int k;
    if (vertDir) // Go vertically
      {
        for (k = 0; k < n_; k++)
          if (filledFlag_(k, j) && k != i && !visitFlagVert_(k, j))
            {
              S.push(Cell(k, j, true, counter));
              visitFlagVert_(k, j) = true;
            }
      }
    else        // Horizontally
      {
        for (k = 0; k < m_; k++)
          if (filledFlag_(i, k) && k != j && !visitFlagHor_(i, k))
            {
              S.push(Cell(i, k, false, counter));
              visitFlagHor_(i, k) = true;
            }
      }
  }


private:
  // Problem input data
  Matrix costs_;
  Vector needs_;
  Vector storage_;

  // Solved plan
  Matrix plan_;
  MatrixInt filledFlag_;

  // Potentials
  Vector u_, v_;
  VectorBool filledU_, filledV_;

  std::map<int, Cell> cycle_;
  MatrixInt visitFlagVert_;
  MatrixInt visitFlagHor_;

  // Dimensions
  int n_;
  int m_;
};
