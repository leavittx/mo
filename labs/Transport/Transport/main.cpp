#include <cstdio>
#include <vector>
#include <transport.h>

using namespace std;

int main()
{
  FILE *F = fopen("input.txt", "rt");

  if(F == NULL)
    return false;
  int N, M;
  std::vector<double> Costs, Storage, Needs;

  fscanf(F, "%d %d", &N, &M);
  Costs.resize(N * M), Storage.resize(M), Needs.resize(N);
  for(int i = 0; i < N; ++i)
  {
    for(int j = 0; j < M; ++j)
      fscanf(F, "%lf", &Costs[i * M + j]);
    fscanf(F, "%lf", &Storage[i]);
  }
  for(int i = 0; i < M; ++i)
    fscanf(F, "%lf", &Needs[i]);
  fclose(F);
  
  Matrix<double> costs(&Costs[0], N, M);
  Vector<double> storage(&Storage[0], N);
  Vector<double> needs(&Needs[0], M);
  TransportProblem<double> ttask(costs, needs, storage);
  ttask.solve();

  return 0;
}

#if 0
#include <OptMethods>
#include <transport.h>

// Program entry point
int main()
{
    // load data
    
    FILE * f = fopen("input.txt", "rt");
    
    if (f == NULL)
        return 1;
    
    int N, M;
    double Costs[100];
    double Storage[100];
    double Needs[100 * 100];
    
    fscanf(f, "%d", &N);
    fscanf(f, "%d", &M);
    
    if (N <= 0 || N >= 100 || M <= 0 || M >= 100)
        return 1;
        
    int i, j;
    for (i = 0; i < N; i++)     
        for (j = 0; j < M; j++)     
            fscanf(f, "%lf", &Costs[i * M + j]);
    for (i = 0; i < N; i++)     
        fscanf(f, "%lf", &Storage[i]);
    for (i = 0; i < M; i++)     
        fscanf(f, "%lf", &Needs[i]);
    
    fclose(f);

    Matrix<double> costs(Costs, N, M);
    Vector<double> storage(Storage, N);
    Vector<double> needs(Needs, M);
    
    // solve
    
    TransportProblem<double> problem(costs, needs, storage);
    problem.Solve();
}
#endif
