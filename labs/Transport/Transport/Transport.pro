SOURCES += \
    main.cpp \
    transport.cpp

HEADERS += \
    transport.h

OTHER_FILES += \
    input.txt

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../unfinished/OptMethods-build-desktop__/release/ -lOptMethods
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../unfinished/OptMethods-build-desktop__/debug/ -lOptMethods
else:symbian: LIBS += -lOptMethods
else:unix: LIBS += -L$$PWD/../../unfinished/OptMethods-build-desktop__/ -lOptMethods

INCLUDEPATH += $$PWD/../../unfinished/OptMethods
DEPENDPATH += $$PWD/../../unfinished/OptMethods-build-desktop__
