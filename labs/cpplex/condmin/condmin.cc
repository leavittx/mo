#include "condmin.h"

#include "variable.h"       
#include <iostream>    
#include <fstream>
#include <cstdlib>
#include <sstream>
#include <algorithm>
#include <cstdio>
#include <cmath>
#include "sys/stat.h"

#define TOL 0.00000000000000000001
#define VERBOSE 0
/// FIXME
#ifdef INFINITY
    #undef INFINITY
    #define INFINITY 1.e+25
#endif

using std::vector;         
using std::string;    
using std::stringstream;
using std::ifstream;
using pilal::tol_equal;

namespace optimization {

/*
        CondMin
        =======
        Implementation of the class that will incapsulate the
        convex comditional minimization problem solving behavior.
    */

Condmin::Condmin(char const * name)
    : name(name),
      solution_dimension(0),
      eps(0.01) {
}     


Condmin::~Condmin() {                                          
}            


//void Condmin::add_variable(Variable* variable) {
//    variables.push_back(variable);
//}

//bool Condmin::has_solutions() const {
//    return !overconstrained;
//}                        

//bool Condmin::is_unlimited() const {
//    return unlimited;
//}

//bool Condmin::must_be_fixed() const {
//    return has_to_be_fixed;
//}


void Condmin::load_problem(char const * problem_name) {

    solution_dimension = 2;
    Matrix coefficients(1, solution_dimension);
    long double bound;
    
    /// Set constraints
    // x - y <= -40 && -7 x + y <= 25 && 2 x + y <= 600
    coefficients(0) =    1.0;
    coefficients(1) =   -1.0;    
    bound = -40.0;
    add_constraint( Constraint( coefficients, CT_LESS_EQUAL, bound ) );
    
    coefficients(0) =   -7.0;
    coefficients(1) =    1.0;
    bound = 25.0;
    add_constraint( Constraint( coefficients, CT_LESS_EQUAL, bound ) );
    
//    coefficients(0) =    0.0;
//    coefficients(1) =    1.0;
//    bound = 100.0;
//    add_constraint( Constraint( coefficients, CT_MORE_EQUAL, bound ) );
    
    coefficients(0) =    2.0;
    coefficients(1) =    1.0;
    bound = 600.0;
    add_constraint( Constraint( coefficients, CT_LESS_EQUAL, bound ) );
    
    /// FIXME: define objective function here somehow
    
    Matrix ip(1, solution_dimension);
    initialPoint = ip;
    /// First
    initialPoint(0) = 70;
    initialPoint(1) = 380;
    /// Second
    initialPoint(0) = 100;
    initialPoint(1) = 141;
}


void Condmin::add_constraint ( Constraint const & constraint ) {
    
    if ( constraints.size() != 0 ) {
        if ( solution_dimension != constraint.coefficients.dim().second )
            throw(DataMismatchException("Constraints must have the same size"));
    } else {
        solution_dimension = constraint.size();
    }
    
    if ( constraint.type == CT_NON_NEGATIVE ) {
        nn_constraints.push_back(constraint);
    } else {
        constraints.push_back(constraint);
    }
}


void Condmin::set_objective_function ( NonLinearObjectiveFunction const& objective_function ) {
    
    if ( solution_dimension != objective_function.costs.dim().second )
        throw(DataMismatchException("Objective function must have same size as solution"));
    
    this->objective_function = objective_function;
}

//void Condmin::log() const {
//}

void Condmin::print_solution() const {
    
    std::cout << "Optimal solution is:" << std::endl;
//    for (int i = 0; i < solution_dimension; ++i)
//        std::cout << variables.at(i)->name << ":\t\t\t" << solution(i) << std::endl;
    
    std::cout << std::endl;
    std::cout << "Solution value/cost:\t\t" << solution_value << std::endl;
}


void Condmin::solve() {
    step = 0;
    Matrix x = getInitialPoint();
    Matrix y(1, solution_dimension), s(1, solution_dimension), g(1, solution_dimension), xp(1, solution_dimension);
    
    long double f_val = objective_function.get_value(x)(0), nu = -INFINITY;
    
    if (!isPointConstrained(x))
        throw(DataMismatchException("Infeasible initial point!"));
    
    std::cout << "Initial objective function value: " << f_val << std::endl;
    
#if 0
    do{
        Point g = new Point(f.getGrad(xk.toArray()));
        yk = new Point(Simplex.simplex(lr, g.toArray()));
        n = g.mul(yk.minus(xk));
        Point sk = yk.minus(xk);
        double a = d;

        while (f.calc(xk.plus(sk.mul(a)).toArray()) - f.calc(xk.toArray()) > a*n/2){
            a *= l;
        }
        xp = xk;
        xk = xk.plus(sk.mul(a));
        System.out.println(f.calc(xk.toArray()) + "   " + xk);
        ++k ;
    }while(xk.eps(xp) > e && k < 1500);
#endif
    long double alpha = 0.99, l = 0.99;
    do {
        g = objective_function.get_grad(x);
        y = auxSolveBySimplex(x);
        f_val = objective_function.get_value(y)(0);
        y.transpose();
        s = y - x;
        nu = dot(g, s);
        
        while (objective_function.get_value(x + alpha * s)(0) -
               objective_function.get_value(x)(0) > alpha*nu/2){
            alpha *= l;
        }
        xp = x;
        x = x + alpha * s;
//        f_val = objective_function.get_value(x)(0);
#if 0
       /// Find "optimal"
       y = auxSolveBySimplex(x);
       y.transpose();
       /// Get direction
       s = y - x;
       /// Calc nu (as dot product)
       nu = dot(s, objective_function.get_grad(x));
       /// Assert on this
       if (!(nu <= 0.0)) {
           std::cout << "(nu <= 0.0) not satisfied" << std::endl;
       }
       /// Try to minimize by ray
       long double alpha = rayMinimize(x, s);
       /// Save point
       x = x + alpha * s;
       f_val = objective_function.get_value(x)(0);
#endif
       
       /// Prepare for the next step       
       step++;
       /// Print
       std::cout << "Reduced objective function value: " << f_val << std::endl;
//       printf("step=%02d: x=(%2.6lf,%2.6lf,%2.6lf,%2.6lf), f=%4.5lf, nu=%2.6lf", step, x[0], x[1], x[2], x[3], f_val, nu);
//       printf(", diff=%1.7lf\n", alpha * s.Length());
        
       if (step == 1)
           break;
    } while(dist(x, xp) > eps); /*while (nu <= -eps);*/
//    } while (dot(g, x - xp) <= -eps);
 
    /// Save optimal point
    solution = x;
 
    /// Save optimal function value
    solution_value = f_val;
}


Matrix Condmin::getInitialPoint() const {
    return initialPoint;
}


bool Condmin::isPointConstrained( Matrix const & x ) const {
    if (x.dim().second != solution_dimension)
        throw(DataMismatchException("Wrong initial point dimension!"));
    
    /// Check if all of the constraints satisfied
    for (int i = 0; i < static_cast<int>(constraints.size()); ++i) {
        if (!constraints[i].isSatisfied(x))
            return false;
    }
    
    return true;
}


Matrix Condmin::auxSolveBySimplex( Matrix const & x ) {
    char str[20];
    sprintf(str, "aux%d", step);    
    Simplex aux(str);
    
    /// Gen linearized task
    /// load_problem() imitation
    
    /// PB_VARS
    
    for (int current_var = 0; current_var < solution_dimension; ++current_var) {
        // Add variable for tracking
        sprintf(str, "x%d", current_var);
        aux.add_variable(new Variable(&aux, const_cast<const char *>(str)));;
        
        // Set variable bounds
        /// FIXME: not using this code anyway
//        string lower_bound = "inf", upper_bound = "inf";
        
//        Matrix eye(1, solution_dimension, 0.0);    
//        eye(current_var) = 1.0;

//        if ( lower_bound != "inf" ) {
//            if ( atof(lower_bound.c_str()) == 0 )
//                add_constraint( Constraint( eye, CT_NON_NEGATIVE, 0 ) );    
//            else
//                add_constraint( Constraint( eye, CT_MORE_EQUAL, atof(lower_bound.c_str()) ) );
//        }
//        if ( upper_bound != "inf")
//            add_constraint( Constraint( eye, CT_LESS_EQUAL, atof(upper_bound.c_str()) ) );
    }
    
    /// PB_CONSTRAINTS

//    aux.constraints = constraints;    
    for (int i = 0; i < static_cast<int>(constraints.size()); ++i) {
        aux.add_constraint(constraints[i]);
    }

//    Matrix coefficients(1, solution_dimension);
//    long double bound;    
    
    // Build conditional matrix and right part vector
//    for (int i = 0; i < static_cast<int>(constraints.size()); ++i) {
////        A[i] = -(_constraints[i].grad)(x0);
//        coefficients(0) = -constraints[i].get_grad(x)(0);
//        coefficients(1) = -constraints[i].get_grad(x)(1);
//        //b[i] = A[i] & x0;
//        bound = dot(coefficients, x);
//        //b[i] += (_constraints[i].func)(x0);
//        bound += constraints[i].get_value(x);
//        add_constraint( Constraint( coefficients, CT_LESS_EQUAL, bound ) );
//    }

    /// PB_OBJECTIVE
    
    Matrix costs(1, solution_dimension);
    costs = objective_function.get_grad(x);
//    costs(0) = -costs(0);
//    costs(1) = -costs(1);
    aux.set_objective_function( ObjectiveFunction( OFT_MINIMIZE, costs) );
    
    /// Run simplex method
    aux.solve();
    /// Show what it has ended with
    std::cout << std::endl;
    if (aux.must_be_fixed()) {
        std::cout << "Aux problem formulation is incorrect." << std::endl;
    }
    if ( aux.has_solutions() ) {
        if ( !aux.is_unlimited() ) 
            aux.print_solution();
        else
            std::cout << "Aux problem is unlimited." << std::endl;
        
    } else {
        std::cout << "Aux problem is overconstrained." << std::endl;
    }
    /// Ok, we've done it
    return aux.solution;
}


// 1D minimization on segment [point; point+dir]
long double Condmin::rayMinimize( Matrix const & point, Matrix const & dir )
{
   const long double alpha = 0.5 * (sqrt(5.0) - 1.0);
   long double lambda, mu, f_lambda, f_mu, a = 0, b = 1;

   const double epsilon = 0.000001;

   if (point.dim().second != solution_dimension)
       throw(DataMismatchException("Wrong point dimension!"));
   if (dir.dim().second != solution_dimension)
       throw(DataMismatchException("Wrong dir dimension!"));

   /// first fill
   lambda = 1.0 - alpha;
   mu = 1.0 - lambda;
   f_lambda = objective_function.get_value(point + lambda * dir)(0);
   f_mu = objective_function.get_value(point + mu * dir)(0);

   /// main method loop
   while (true) {
      /// Decide about direction
      if (f_lambda < f_mu) {
         /// Change segment
         b = mu;
         if (fabs(b - a) <= epsilon) {
            break;
         }
         /// Recalculate values
         mu = lambda;
         f_mu = f_lambda;
         lambda = a + (1.0 - alpha) * (b - a);
         f_lambda = objective_function.get_value(point + lambda * dir)(0);
      } else {
         /// Change segment
         a = lambda;
         if (fabs(b - a) <= epsilon) {
            break;
         }
         /// Recalculate values
         lambda = mu;
         f_lambda = f_mu;
         mu = a + alpha * (b - a);
         f_mu = objective_function.get_value(point + mu * dir)(0);
      }
   }

   /// Exit with success
   return mu;
}


} // end of 'optimization' namespace

