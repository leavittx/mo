#pragma once

// PILAL
#include "pilal.h"

// Simplex classes
#include "simplexexceptions.h"
#include "constraint.h"
#include "objectivefunction.h"
#include "columnset.h"


// From the STL
#include <iostream>
#include <vector>

// Using
using pilal::Matrix;
using pilal::AnonymousMatrix;

namespace optimization {

class ColumnSet;
class Constraint;
class ObjectiveFunction;
class Variable;

class NonLinearObjectiveFunction : public ObjectiveFunction
{
public:
    NonLinearObjectiveFunction()
        : value(1, 1), grad(1,2) {
//        e(1);
//        e.set_identity();
//        value = Matrix(1, 1);
//        grad  = Matrix(1, 2);
    }
    
    virtual Matrix const & get_value( Matrix const & x ) {
        value(0) = x(0) * x(0) + 2.0 * x(1) * x(1) - x(1);
//        value(0) = x(0)*x(0) + x(1)*x(1) - 6.0*x(0) - 3.0*x(1);
        std::cout << "get_value: x(" << x(0) << ")	y(" << x(1) << ")   	";
        std::cout << "f(x, y) = " << value(0) << std::endl;
        return value;
    }
    
    virtual Matrix const & get_grad( Matrix const & x ) {
        grad(0) = 2.0 * x(0);
        grad(1) = 4.0 * x(1) - 1.0;
//        grad(0) = 2*x(0) - 6;
//        grad(1) = 2*x(1) - 3;
        return grad;
    }
    
private:
//    Matrix e;
    Matrix value;
    Matrix grad;
};

class Condmin
{
public:
    // Constructor
    Condmin(char const * name);
    ~Condmin();
    
    // Settings                                
    void load_problem(char const * problem_name);
    void add_variable(Variable* variable);
    void add_constraint(Constraint const & constraint);
    void set_objective_function(NonLinearObjectiveFunction const & objective_function);
    void setAccuracy(long double epsilon) { eps = epsilon; }
    
    // Solving procedures
    void solve();
    Matrix getInitialPoint() const;
    bool isPointConstrained( Matrix const & x ) const;
    Matrix auxSolveBySimplex( Matrix const & x );
    long double rayMinimize( Matrix const & point, Matrix const & dir );
    
    // Print
    void print_solution() const;
    void log() const; 
    
    //    bool is_unlimited() const;
    //    bool has_solutions() const;
    //    bool must_be_fixed() const;
    //    Matrix const & get_dual_variables() const;

    
    
protected:
    
    std::string name;
    
    // Data
    NonLinearObjectiveFunction objective_function;
    std::vector< Constraint> constraints;
    std::vector< Constraint> nn_constraints;
    std::vector< Variable*> variables;
    
    // Results
    Matrix solution;
    long double solution_value;
    
    int solution_dimension;
    
    long double eps;
    int step;
    
    Matrix initialPoint;
    
    //    bool    optimal, 
    //            unlimited, 
    //            overconstrained, 
    //            has_to_be_fixed, 
    //            changed_sign;
};

}
