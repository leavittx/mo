#include <iostream>
#include "pilal.h"
#include "simplex.h"
#include "condmin.h"

using namespace pilal;
using namespace optimization;

int main( int argc, char* argv[] ) {
    Condmin problem("");

    try {                                
        problem.load_problem("Conditional min problem");
        problem.solve();         
        std::cout << std::endl;
        
//        if (problem.must_be_fixed()) {
//            std::cout << "Problem formulation is incorrect." << std::endl;
//            return 1;
//        }
        
//        if ( problem.has_solutions() ) {
//            if ( !problem.is_unlimited() ) 
                problem.print_solution();
//            else
//                std::cout << "Problem is unlimited." << std::endl;
            
//        } else {
//            std::cout << "Problem is overconstrained." << std::endl;
//        }                                                           
        
    } catch ( DataMismatchException c ) {
        std::cout << "Error: " << c.error << std::endl;
    } 
    
    return 0;    
}
