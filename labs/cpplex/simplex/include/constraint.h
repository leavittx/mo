/*
This file is part of C++lex, a project by Tommaso Urli.

C++lex is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

C++lex is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with C++lex.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef CONSTRAINT_H
#define CONSTRAINT_H

#include "pilal.h"
using pilal::Matrix;

#include "simplexexceptions.h"
using pilal::tol_equal;

namespace optimization {

    enum ConstraintType {
        
        CT_LESS_EQUAL,
        CT_MORE_EQUAL,
        CT_EQUAL,
        CT_NON_NEGATIVE,
        CT_BOUNDS     
           
    };
        
            
    class Constraint {
        
        friend class Simplex;
        friend class Condmin;
        
        public:
                
            Constraint( Matrix const & coefficients, ConstraintType type, long double value );
            Constraint( Matrix const & coefficients, ConstraintType type, long double lower, long double upper );
            
            // Debug
            void log() const; 
            void add_column(long double value);
            int size() const; 
            bool isSatisfied( Matrix const & point ) const {
                if (point.dim() != coefficients.dim())
                    throw(DataMismatchException("Wrong point dimension!"));
                
                Matrix left_part(1, 1);
                left_part(0) = dot(coefficients, point);
                if (type == CT_LESS_EQUAL) {
                    return left_part.less_equal_than(upper, TOL);
                }
                if (type == CT_MORE_EQUAL) {
                    return left_part.more_equal_than(lower, TOL);
                }
                if (type == CT_EQUAL) {
                    return tol_equal(left_part, value, TOL);
                }
                
                /// FIXME: more types
                
                return true;
            }
            Matrix const & get_grad( Matrix const & x ) {
                grad(0) = coefficients(0) * x(0);
                grad(1) = coefficients(1) * x(1);
                return grad;
            }
            
        private:
                
            ConstraintType type;
            Matrix coefficients;
            long double value;
            long double upper;
            long double lower;        
            
            Matrix grad;
    };

}

#endif
