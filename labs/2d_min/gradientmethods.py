from pylab import *

def pointTransform(x):
    x1 = [0.0, 0.0]
    phi = -3.1415926535 / 3.6 # 45 degrees
    x1[0] = x[0] * cos(phi) - x[1] * sin(phi)# / 3.0
    x1[1] = x[0] * sin(phi) + x[1] * cos(phi)
    return x1

def function(x):
    #return exp(2.0 * x[0] * x[0] + 3.0 * x[1] * x[1]) + 1.0 * sin(0.5 * (x[0] - x[1]))
    #x = pointTransform(x)
    return 2 * x[0] * x[0] * x[0] * x[0] + x[1] * x[1] * x[1] * x[1] + 2 * x[0] * x[1] - 4


def functionGradient(x):
    #x = pointTransform(x)
    gradient = [0.0, 0.0]
    #gradient[0] = 4.0 * x[0] * exp(2.0 * x[0] * x[0] + 3.0 * x[1] * x[1]) + 0.5 * cos(0.5 * (x[0] - x[1]))
    #gradient[1] = 6.0 * x[1] * exp(2.0 * x[0] * x[0] + 3.0 * x[1] * x[1]) - 0.5 * cos(0.5 * (x[0] - x[1]))
    gradient[0] = 8 * x[0] * x[0] * x[0] + 2 * x[1]
    gradient[1] = 4 * x[1] * x[1] * x[1] + 2 * x[0]
    return gradient

def functionHessian(x):
    #a = 4.0 * (4.0 * x[0] * x[0] + 1.0) * exp(2.0 * x[0] * x[0] + 3.0 * x[1] * x[1]) - 0.25 * sin(0.5 * (x[0] - x[1]))
    #b = 24.0 * x[0] * x[1] * exp(2.0 * x[0] * x[0] + 3.0 * x[1] * x[1]) + 0.25 * sin(0.5 * (x[0] - x[1]))
    #c = 6.0 * (6.0 * x[1] * x[1] + 1.0) * exp(2.0 * x[0] * x[0] + 3.0 * x[1] * x[1]) - 0.25 * sin(0.5 * (x[0] - x[1]))
    #x = pointTransform(x)
    a = 24 * x[0] * x[0]
    b = 2
    c = 12 * x[1] * x[1]

    hessian = [[0, 0], [0, 0]]
    hessian[0][0] = a
    hessian[0][1] = b
    hessian[1][0] = b
    hessian[1][1] = c
    return hessian    

def vectorSum(x, y):
    return [a + b for a, b in zip(x, y)]


def dotProduct(x, y):
    return sum(a * b for a, b in zip(x, y))


def vectorMultiply(x, factor):
    return [a * factor for a in x]


def vectorNorm(x):
    return sqrt(dotProduct(x, x))

def matrixMultiplyVector(m, x):
    result = [0.0, 0.0]
    result[0] = m[0][0] * x[0] + m[0][1] * x[1]
    result[1] = m[1][0] * x[0] + m[1][1] * x[1]
    return result    

def invertMatrix(m):
    a = m[0][0]
    b = m[0][1]
    c = m[1][0]
    d = m[1][1]

    det = (a * d - b * c)
    assert det != 0

    result = [[d * 1.0 / det, -b * 1.0 / det], [-c * 1.0 / det, a * 1.0 / det]]
    return result

def gradientMethodWithStepSubdivision(f, initialX, precision):
    def _chooseStep(x, f, direction, initialStep, eps, subdivisionFactor):
        step = initialStep
        gradient = functionGradient(x)
        factor = -eps * dotProduct(gradient, gradient)
        while f(vectorSum(x, vectorMultiply(direction, step))) - f(x) > step * factor:
            step *= subdivisionFactor
        return step

    eps = 0.5
    subdivisionFactor = 0.5
    initialStep = 1.0
    points = [initialX]

    iterationsNumber = 0
    x = initialX
    gradient = functionGradient(x)
    antiGradient = vectorMultiply(gradient, -1.0)
    step = _chooseStep(x, f, antiGradient, initialStep, eps, subdivisionFactor)

    while abs(f(vectorSum(x, vectorMultiply(antiGradient, step))) - f(x)) > precision:
        antiGradient = vectorMultiply(gradient, -1.0)
        step = _chooseStep(x, f, antiGradient, initialStep, eps, subdivisionFactor)
        x = vectorSum(x, vectorMultiply(antiGradient, step))
        gradient = functionGradient(x)
        points.append(x)
        iterationsNumber += 1

    return x, iterationsNumber, points


def gradientMethodWithQuickestDescent(f, initialX, precision):
    def _chooseStepWithGoldenSectionMethod(a, b, f, precision, currentX, currentDirection):
        factor = 0.5 * (3.0 - sqrt(5))
        segmentLeftBound = a
        segmentRightBound = b
        segmentLength = segmentRightBound - segmentLeftBound

        while segmentLength >= precision:
            y = segmentLeftBound + factor * segmentLength
            z = segmentRightBound - factor * segmentLength

            if f(vectorSum(currentX, vectorMultiply(currentDirection, y))) <= \
               f(vectorSum(currentX, vectorMultiply(currentDirection, z))):
                segmentRightBound = z
            else:
                segmentLeftBound = y

            segmentLength = segmentRightBound - segmentLeftBound

        return (segmentLeftBound + segmentRightBound) / 2

    stepLeftBound = 0
    stepRightBound = 5
    points = [initialX]

    iterationsNumber = 0
    x = initialX
    gradient = functionGradient(x)
    antiGradient = vectorMultiply(gradient, -1.0)
    step = _chooseStepWithGoldenSectionMethod(stepLeftBound, stepRightBound, f, precision, x, antiGradient)

    while abs(f(vectorSum(x, vectorMultiply(antiGradient, step))) - f(x)) > precision:
        antiGradient = vectorMultiply(gradient, -1.0)
        step = _chooseStepWithGoldenSectionMethod(stepLeftBound, stepRightBound, f, precision, x, antiGradient)
        x = vectorSum(x, vectorMultiply(antiGradient, step))
        gradient = functionGradient(x)
        points.append(x)
        iterationsNumber += 1

    return x, iterationsNumber, points

def newtonGradientMethodWithStepSubdivision(f, initialX, precision):
    def _chooseStep(x, f, direction, initialStep, eps, subdivisionFactor):
        step = initialStep
        gradient = functionGradient(x)
        factor = eps * dotProduct(gradient, direction)
        while f(vectorSum(x, vectorMultiply(direction, step))) - f(x) > step * factor:
            step *= subdivisionFactor
        return step

    eps = 0.5
    subdivisionFactor = 0.5
    initialStep = 1.0
    points = [initialX]

    iterationsNumber = 0
    x = initialX
    gradient = functionGradient(x)
    invertedHessian = invertMatrix(functionHessian(x))
    antiGradient = vectorMultiply(gradient, -1.0)
    direction = matrixMultiplyVector(invertedHessian, antiGradient)
    step = _chooseStep(x, f, direction, initialStep, eps, subdivisionFactor)

    while abs(f(vectorSum(x, vectorMultiply(direction, step))) - f(x)) > precision:
        invertedHessian = invertMatrix(functionHessian(x))
        antiGradient = vectorMultiply(gradient, -1.0)
        direction = matrixMultiplyVector(invertedHessian, antiGradient)
        step = _chooseStep(x, f, direction, initialStep, eps, subdivisionFactor)
        x = vectorSum(x, vectorMultiply(direction, step))
        gradient = functionGradient(x)
        points.append(x)

        iterationsNumber += 1

    return x, iterationsNumber, points


def proveLinearityForSubdivisionMethod(f, exactSolution, points):
    factors = []

    for i in range(len(points) - 2):
        factor = (f(points[i + 1]) - f(exactSolution)) / (f(points[i]) - f(exactSolution))
        factors.append(factor)

    averageFactor = sum(factors) / len(factors)
    return averageFactor, factors

def proveApplicability(a, b, llr, rlr):
    L = np.arange(llr, rlr, 0.001)
    X = Y = np.arange(a, b, 0.1)
    args  = [[x, y] for x, y in zip(X, Y)]
    for l in range(0, len(L)):
        anyStopped = False
        for i in range(0, len(args)):
            stop = False
            for j in range(1, len(args)):
                x = args[i]
                y = args[j]
                gradientX = functionGradient(x)
                gradientY = functionGradient(y)
                if vectorNorm(vectorSum(gradientX, vectorMultiply(gradientY, -1) )) > \
                   L[l] * vectorNorm(vectorSum(x, vectorMultiply(y, -1))):
                    stop = True
            if stop:
                anyStopped = True
                break
        if not anyStopped:
            print 'Applicability is proved. Lipchitz constant:', L[l]
            return True

    return False

def drawPlot(plotName, f, points):
    figure(plotName, dpi = 100)
    spectral()
    #X = arange(-1.0, -0.4, 0.01)
    #Y = arange(0.1, 0.85, 0.01)
    X = arange(-2.0, 2.0, 0.01)
    Y = arange(-2.0, 2.0, 0.01)
    X, Y = meshgrid(X, Y)
    Z = [f([x, y]) for x, y in zip(X, Y)]
    contour(X, Y, Z, 75)

    xOld = points[0]
    for x in points:
        a = [xOld[0], x[0]]
        b = [xOld[1], x[1]]
        plot(a, b, color='blue', linestyle='solid')
        xOld = x
        plot(x[0], x[1], 'b.')
    plot(xOld[0], xOld[1], 'r.')

    savefig(plotName)

def solveMinimizationProblem(method, f, initialX, precision):
    (result, iterationsNumber, points) = method(f, initialX, precision)
    print 'Precision:', precision
    print 'Result: (', result[0], ',', result[1], ')'
    print 'Function value: ', function(result)
    print 'Steps number:', iterationsNumber
    print

    return points


if __name__ == '__main__':
    #precisions = [0.1, 0.01, 0.001]
    precisions = [0.001]
#   initialX = [0.3, 0.4]
    #initialX = [-0.9, 0.5] # ok for my func
    initialX = [-0.95, 0.95]
    #initialX = [-1.5, 1.5]

    #applicabilityIsProved = proveApplicability(-0.5, 0.5, 40, 45)
    #if not applicabilityIsProved:
        #print 'Applicability of gradient methods is not proved'
        #exit()
    #print

    print 'Gradient method with step subdivision:'
    for precision in precisions:
        points = solveMinimizationProblem(gradientMethodWithStepSubdivision, function, initialX, precision)
        precisionString = "%.3f" % precision
        precisionString = precisionString.replace(".", ",")
        drawPlot('Step_subdivision_' + precisionString, function, points)

    print 'Quickest descent gradient method:'
    for precision in precisions:
        points = solveMinimizationProblem(gradientMethodWithQuickestDescent, function, initialX, precision)
        precisionString = "%.3f" % precision
        precisionString = precisionString.replace(".", ",")
        drawPlot('Quickest_descent_' + precisionString, function, points)

    print 'Newton gradient method with step subdivision:'
    for precision in precisions:
        points = solveMinimizationProblem(newtonGradientMethodWithStepSubdivision, function, initialX, precision)
        precisionString = "%.3f" % precision
        precisionString = precisionString.replace(".", ",")
        drawPlot('Newton_' + precisionString, function, points)

    print 'Rate of convergence linearity proof for subdivision method for eps = 0.001:'
    points = gradientMethodWithStepSubdivision(function, initialX, 0.001)[2]
    exactResult = gradientMethodWithQuickestDescent(function, initialX, 1e-10)[0]
    (averageFactor, factors) = proveLinearityForSubdivisionMethod(function, exactResult, points)
    print 'Factors:', factors
    print 'Average factor:', averageFactor
    print

    #print 'Quickest descent gradient method with optimal direction:'
    #optimalInitX = [0.3, 0.05]
    #points = solveMinimizationProblem(gradientMethodWithQuickestDescent, function, optimalInitX, 0.001)
    #drawPlot('Optimal direction', function, points)

