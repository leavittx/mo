#include <stdio.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>

//#define SASHA

double eps = 1e-3;


template<int a> double pow( double x ) 
{
    double y = 1;
    for (int i = 0; i < a; ++i)
        y *= x;
    return y;
//    return exp(a * log(x));
}

double pow( double x, double a ) 
{
   return exp(a * log(x));
}


double Function( double x1, double x2 )
{
//    double x1_2 = x1 * x1, x2_2 = x2 * x2;
//    return x1_2 * x1_2 + 2 * x2_2 * x2_2 + x1_2 * x2_2 + 2 * x1 + x2;
    
//    double x1_4 = x1 * x1 * x1 * x1, x2_4 = x2 * x2 * x2 * x2;
//    return 2 * x1_4 + x2_4 + 2 * x1 * x2 - 4;
    
#ifndef SASHA
    return 2 * pow<4>(x1) + pow<4>(x2) + 2 * x1 * x2 - 4;
#else
    double x[] = { x1, x2 };
    return exp(2.0 * x[0] * x[0] + 3.0 * x[1] * x[1]) + 1.0 * sin(0.5 * (x[0] - x[1]));
#endif
}


void Gradient( double x[], double y[] )
{
//    y[0] = 4 * x[0] * x[0] * x[0] + 2 * x[0] * x[1] * x[1] + 2;
//    y[1] = 8 * x[1] * x[1] * x[1] + 2 * x[1] * x[0] * x[0] + 1;
    
//    double x1_3 = x[0] * x[0] * x[0], x2_3 = x[1] * x[1] * x[1];
//    y[0] = 8 * x1_3 + 2 * x[1];
//    y[1] = 4 * x2_3 + 2 * x[0];
    
#ifndef SASHA
    y[0] = 8 * pow<3>(x[0]) + 2 * x[1];
    y[1] = 4 * pow<3>(x[1]) + 2 * x[0];
#else    
    y[0] = 4.0 * x[0] * exp(2.0 * x[0] * x[0] + 3.0 * x[1] * x[1]) + 0.5 * cos(0.5 * (x[0] - x[1]));
    y[1] = 6.0 * x[1] * exp(2.0 * x[0] * x[0] + 3.0 * x[1] * x[1]) - 0.5 * cos(0.5 * (x[0] - x[1]));
#endif
}


void Hessian( double x[], double H[2][2] )
{
//    double a = 12 * x[0] * x[0] + 2 * x[1] * x[1],
//           b = 4 * x[0] * x[1],
//           c = 24 * x[1] * x[1] + 2 * x[0] * x[0];
#ifndef SASHA
    double a = 24 * pow<2>(x[0]),
           b = 2,
           c = 12 * pow<2>(x[1]);
#else    
    double a = 4.0 * (4.0 * x[0] * x[0] + 1.0) * exp(2.0 * x[0] * x[0] + 3.0 * x[1] * x[1]) - 0.25 * sin(0.5 * (x[0] - x[1])),
           b = 24.0 * x[0] * x[1] * exp(2.0 * x[0] * x[0] + 3.0 * x[1] * x[1]) + 0.25 * sin(0.5 * (x[0] - x[1])),
           c = 6.0 * (6.0 * x[1] * x[1] + 1.0) * exp(2.0 * x[0] * x[0] + 3.0 * x[1] * x[1]) - 0.25 * sin(0.5 * (x[0] - x[1]));
#endif
    H[0][0] = a;
    H[0][1] = H[1][0] = b;
    H[1][1] = c;
}


double dot( double x[], double y[] )
{
    return x[0] * y[0] + x[1] * y[1];
}


void mul( double fac, double x[], double y[] )
{
    y[0] = fac * x[0];
    y[1] = fac * x[1];
}


double chooseStepBySubdivision( double x[], double *f, double dir[], double subdivisionFactor )
{
    double step = 1.0, fnew, epsilon = /*eps*/0.5, grad[2], factor, xk[2];
    
    Gradient(x, grad);
    factor = epsilon * dot(grad, dir);
    
    do
    {
        step *= subdivisionFactor;
        
        mul(step, dir, xk);
        fnew = Function(x[0] + xk[0], x[1] + xk[1]);
    } while (fnew - *f > step * factor); 
    
    return step;
}


void Gradient2( double x[], double y[], int recalculate )
{
    static double H[2][2], invH[2][2], grad[2], det;
    
    if (recalculate)
    {
        Hessian(x, H);
        det = H[0][0] * H[1][1] - H[0][1] * H[1][0];
        if (det < 1e-5)
        {
            y[0] = y[1] = 0;
            return;
        }
        invH[0][0] =  H[1][1] / det;
        invH[0][1] = -H[0][1] / det;
        invH[1][0] = -H[1][0] / det;
        invH[1][1] =  H[0][0] / det;
    }
    Gradient(x, grad);
    // Calc final direction
    y[0] = (invH[0][0] * grad[0] + invH[0][1] * grad[1]);
    y[1] = (invH[1][0] * grad[0] + invH[1][1] * grad[1]);
}


double Step_Newton( double x[], double y[], double *f, double norm )
{
//    x[0] -= y[0];
//    x[1] -= y[1];
    x[0] += y[0];
    x[1] += y[1];
    *f = Function(x[0], x[1]);
    return 1;
}


double Step_subdivide( double x[], double y[], double *f, double norm )
{
    double step = 1, fnew, epsilon = eps;
    
    if (norm < eps)
        return 0;
    
    do
    {
        step *= 0.5;
        fnew = Function(x[0] - step * y[0], x[1] - step * y[1]);
    } while (fnew - *f > -epsilon * step * norm); 
    
    x[0] -= step * y[0];
    x[1] -= step * y[1];
    *f = fnew;
    return step;
}


//double lipsh = 9.2;

//double Step_fixed( double x[], double y[], double *f, double norm )
//{
//    double step = (1 - eps) / lipsh;
    
//    x[0] -= step * y[0];
//    x[1] -= step * y[1];
//    *f = Function(x[0], x[1]);
    
//    return step;
//}


double Step_optimal( double x[], double y[], double *f, double norm )
{
    double alpha = (sqrt(5) - 1) / 2;
    double a = 0, b = 1;
    double l, m, fl, fm;
    
    l = a + (1 - alpha) * (b - a);
    m = a + b - l;
    fl = Function(x[0] - y[0] * l, x[1] - y[1] * l);
    fm = Function(x[0] - y[0] * m, x[1] - y[1] * m);
    
    while (fabs(m - l) > .001)
    {
        if (fl < fm)
        {
            b = m;
            m = l;
            fm = fl;
            l = a + (1 - alpha) * (b - a);
            fl = Function(x[0] - y[0] * l, x[1] - y[1] * l);
        }
        else 
        {
            a = l;
            l = m;
            fl = fm;
            m = a + alpha * (b - a);
            fm = Function(x[0] - y[0] * m, x[1] - y[1] * m);
        }
    }
    
    x[0] -= l * y[0];
    x[1] -= l * y[1];
    *f = Function(x[0], x[1]);
    return l;
}


//int period;


int Test( double (*Step)(double[], double[], double *, double), int i )
{
    int    step = 0;
    double x[2] = {/*0.2, 0.3*//*-0.5, 0.6*/1, 1}, x_prev[2], delta[2], old_delta[2], y[2],
            f = Function(x[0], x[1]), norm, mod, prev_mod;
    static double solution[2];
    double antiGradient[2], curStep, fnew;
    
    // Newton method with subdivision
    if (i == 3)
    {
//        Gradient(x, y);
//        mul(-1.0, y, antiGradient);
//        curStep = chooseStepBySubdivision(x, &f, antiGradient, 0.5);
//        printf("Initial step = %6.4lf\n\n", curStep);
        
        printf("x = (% 7.4lf, % 7.4lf) y = (% 7.4lf, % 7.4lf), f=%7.4lf \n",
               x[0], x[1], y[0], y[1], f);
        
        do
        {
//            Gradient2(x, y, 1);
                        
            double H[2][2], invH[2][2], grad[2], det;
            
            Hessian(x, H);
            det = H[0][0] * H[1][1] - H[0][1] * H[1][0];
            if (det < 1e-5)
            {
                y[0] = y[1] = 0;
                printf("det < 1e-5\n");
            }
            
            invH[0][0] =  H[1][1] / det;
            invH[0][1] = -H[0][1] / det;
            invH[1][0] = -H[1][0] / det;
            invH[1][1] =  H[0][0] / det;

            Gradient(x, grad);
            // Anti gradient
            mul(-1.0, grad, grad);
            
            // Calc final direction
            y[0] = (invH[0][0] * grad[0] + invH[0][1] * grad[1]);
            y[1] = (invH[1][0] * grad[0] + invH[1][1] * grad[1]);
            
            curStep = chooseStepBySubdivision(x, &f, y, 0.5);
            
            mul(curStep, y, y);
            Step(x, y, &f, norm);
            
            printf("x = (% 7.4lf, % 7.4lf) y = (% 7.4lf, % 7.4lf), f=%7.4lf ",
                   x[0], x[1], y[0], y[1], f);
            printf("step = %6.4lf\n", curStep);
            
            step++;
            
            fnew = Function(x[0] + y[0], x[1] + y[1]);
        } while (fabs(fnew - f) > eps && step < 15); 
        //while abs(f(vectorSum(x, vectorMultiply(direction, step))) - f(x)) > precision:
        
    }
    else {
    do
    {
        
        Gradient(x, y);
        
        //        if (i == 4)
        //            Gradient2(x, y, (step % period) == 0);
        norm = sqrt(y[0] * y[0] + y[1] * y[1]);
        printf("x = (% 7.4lf, % 7.4lf) y = (% 7.4lf, % 7.4lf) norm = % 7.4lf,"
               " f=%7.4lf ", x[0], x[1], y[0], y[1], norm, f);
        memcpy(x_prev, x, 2 * sizeof(double));
        printf("step = %6.4lf", Step(x, y, &f, norm));
        
        if (i == 1)
        {
            delta[0] = x[0] - solution[0];
            delta[1] = x[1] - solution[1];
            mod = sqrt(delta[0] * delta[0] + delta[1] * delta[1]);
            if (step != 0)
                printf(", |x - x*| / |x_prev - x*| = %lf", mod / prev_mod);
        }
        if (i == 2)
        {
            delta[0] = x[0] - x_prev[0];
            delta[1] = x[1] - x_prev[1];
            printf(", delta = (%8.5lf, %8.5lf)", delta[0], delta[1]);
            if (step != 0)
                printf(", delta * old_delta = %.5lf", delta[0] * old_delta[0] +
                       delta[1] * old_delta[1]);
        }
        printf("\n");

        old_delta[0] = delta[0];
        old_delta[1] = delta[1];
        prev_mod = mod;
        
        step++;
        
    } while (norm > eps);
    }
    
    if (i == 0)
        memcpy(solution, x, 2 * sizeof(double));
    printf("Done in %d steps\n", step);
    return step;
}


//double FindLipshitzConstant( double left, double right, double bottom, double top )
//{
//    double R, R_max = 0;
//    double t, x1, x2, y1, y2, dx, dy, df, denom;
    
//    while (1)
//    {
//        t = (double)(rand() % 100) / 100.0;
//        x1 = left + (1 - t) + right * t;
//        t = (double)(rand() % 100) / 100.0;
//        x2 = left + (1 - t) + right * t;
//        t = (double)(rand() % 100) / 100.0;
//        y1 = bottom + (1 - t) + top * t;
//        t = (double)(rand() % 100) / 100.0;
//        y2 = bottom + (1 - t) + top * t;
        
//        //printf("%lf, %lf, %lf, %lf\n", x1, x2, y1, y2);
//        dx = x2 - x1;
//        dy = y2 - y1;
//        df = fabs(Function(x2, y2) - Function(x1, y1));
        
//        denom = sqrt(dx * dx + dy * dy);
//        if (denom < 1e-5)
//            continue;
//        R = df / denom;
//        if (R > R_max)
//            R_max = R;
//        printf("%lf\n", R_max);
//    }
//}


int main( void ) 
{
    //FindLipshitzConstant(-1, 0, -1, 0);
    printf("\n2-dimensional minimization example\n");
//    printf("\nFixed step:\n");
//    Test(Step_fixed, 0);
    
//    printf("TEST: %f\n\n", pow(2.0, 10));
    
    printf("\nStep subdivision:\n");
    Test(Step_subdivide, 1);
    printf("\nOptimal step:\n");
    Test(Step_optimal, 2);
    
    printf("\nSecond order gradient:\n");
    Test(Step_Newton, 3);
    
//    period = 2;
//    for (int i = 0; i < 4; i++)
//    {
//        printf("\nSecond order gradient, with calculating invH once at %d steps:\n",
//               period);
//        Test(Step_Newton, 4);
//        period *= 2;
//    }
    //scanf("%*c");
    return 0;
}
