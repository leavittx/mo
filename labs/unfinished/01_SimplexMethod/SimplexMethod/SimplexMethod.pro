SOURCES += \
    main.cpp

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../OptMethods-build-desktop__/release/ -lOptMethods
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../OptMethods-build-desktop__/debug/ -lOptMethods
else:unix:!macx:!symbian: LIBS += -L$$PWD/../../OptMethods-build-desktop__/ -lOptMethods

INCLUDEPATH += $$PWD/../../OptMethods
DEPENDPATH += $$PWD/../../OptMethods

QMAKE_CXXFLAGS += -fpermissive -Wall

OTHER_FILES += \
    input.txt \
    tasks/task_my.txt \
    tasks/task_lm1.txt
