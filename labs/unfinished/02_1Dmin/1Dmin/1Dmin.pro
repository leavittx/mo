
win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../OptMethods-build-desktop__/release/ -lOptMethods
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../OptMethods-build-desktop__/debug/ -lOptMethods
else:unix:!macx:!symbian: LIBS += -L$$PWD/../../OptMethods-build-desktop__/ -lOptMethods

INCLUDEPATH += $$PWD/../../OptMethods
DEPENDPATH += $$PWD/../../OptMethods

SOURCES += \
    main.cpp
