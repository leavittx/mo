#include <OptMethods>

using namespace std;

///////////////////////////////////////////////////
/// Algorithms and data structures
///////////////////////////////////////////////////

/// Real-variable function
class RealF
{
public:
    virtual real operator()(real x)
    {
//        x = x;
//        return 0;
        return -sin(0.5*x);
//        return x * x * x * x + 8 * x * x * x - 6 * x * x - 72 * x + 90;
    }
};
    

class MyFunction : public RealF
{    
    virtual real operator()(real x)
    {
//        return sin(x);
        return x * x * x * x + 8 * x * x * x - 6 * x * x - 72 * x + 90;
    }
};

/// Range
template <typename T>
struct GenericRange
{
	/// @invariant  Should be "b >= a". Someone can put assertions of this.
	T a, b;
    GenericRange() {}
	GenericRange(const T &newA, const T &newB) { a = newA; b = newB; }
	T length() { return b - a; }
};
/// Closed range
template <typename T>
struct ClosedRange : public GenericRange<T>
{
    ClosedRange() {}
	ClosedRange(const T &newA, const T &newB)
	{
		this->a = newA;
		this->b = newB;
		if (numeric_limits<T>::has_infinity)
        {
			runtimeCheck(this->a != numeric_limits<T>::infinity() && this->a != -numeric_limits<T>::infinity(), 
						 "Infinity isn\'t allowed to enclose ClosedRange");
			runtimeCheck(this->b != numeric_limits<T>::infinity() && this->b != -numeric_limits<T>::infinity(), 
						 "Infinity isn\'t allowed to enclose ClosedRange");
		}
	}
};
/// Real-value closed range
typedef ClosedRange<real> RealClosedRange;


/// Data
struct DataMin1D
{
    DataMin1D(real epsilon, int param = 0)
        : epsilon(epsilon), param(param) {}
    
    real epsilon;
    int param;
    real result;
    real point;
    int nCalls;
};


/// One dimentional function mimimization class    
class Min1D
{
public:
    virtual bool operator()(RealF f, RealClosedRange r, DataMin1D& data) = 0;
};


/// First method: Elena Aleksandrovna search
class EAMin1D : public Min1D
{
public:
    bool operator()(RealF f, RealClosedRange r, DataMin1D& data)
    {
        runtimeCheck(r.a <= r.b, "Invalid range");
        
        nSteps = data.param;
        
        real fa = f(r.a);
        data.nCalls = 1;

        while (r.b - r.a > data.epsilon)
        {
            real step = (r.b - r.a) / nSteps;
            real fx = fa;
            real tmp = fa;
            real min = fa;
            int i = 1;
            for (; i != nSteps; ++i)
            {
                fa  = fx;
                fx  = tmp;
                tmp = f(r.a + i * step);
                data.nCalls++;
                if (tmp < min)
                {
                    min = tmp;
                }
                else if (tmp > min)
                {
                    break;
                }
            }
            if (i == nSteps)
            {
                r.a  = r.b - step;
                fa = fx;
            }
            else
            {
                r.b = r.a + i * step;
                if (i != 1)
                {
                    r.a = r.b - 2 * step;
                }
            }
        }
        
        data.point = (r.a + r.b) / 2;
        data.result = f(data.point);
        
        return true;
    }
private:
    int nSteps;
};


/// Second method: Golden section search
class GSMin1D : public Min1D
{
public:
    bool operator()(RealF f, RealClosedRange r, DataMin1D& data)
    {
        runtimeCheck(r.a <= r.b, "Invalid range");
//        printf("%f\n", goldenRatio);
        
        real y  = r.a + goldenRatio * (r.b - r.a);
        real z  = r.b - goldenRatio * (r.b - r.a);
        real fy = f(y);
        real fz = f(z);
        data.nCalls = 2;
        
        while (r.b - r.a > data.epsilon)
        {
            if (fy <= fz)
            {
                r.b  = z;
                z  = y;
                y  = r.a + goldenRatio * (r.b - r.a);
                fz = fy;
                fy = f(y);
                data.nCalls++;
            }
            else
            {
                r.a  = y;
                y  = z;
                z  = r.b - goldenRatio * (r.b - r.a);
                fy = fz;
                fz = f(z);
                data.nCalls++;
            }
        }
        
        data.point = (r.a + r.b) / 2;
        data.result = f(data.point);
        
        return true;
    }
};


/// Extra method: 1/2 devision search
class HDMin1D : public Min1D
{
public:
    bool operator()(RealF f, RealClosedRange r, DataMin1D& data)
    {
        runtimeCheck(r.a <= r.b, "Invalid range");
        
        data.nCalls = 0;
        
        while (r.b - r.a > data.epsilon)
        {
            double x    = (r.a + r.b) / 2;
            double step = min((r.b - r.a) / 1000, data.epsilon / 2);
            double fl   = f(x - step);
            double fr   = f(x + step);
            data.nCalls += 2;
            if (fl < fr)
            {
                r.b = x;
            }
            else if (fr < fl)
            {
                r.a = x;
            }
            else
            {
                r.a = x - step;
                r.b = x + step;
            }
        }
        
        data.point = (r.a + r.b) / 2;
        data.result = f(data.point);
        
        return true;
    }
};

    
///////////////////////////////////////////////////
/// Problem specific
///////////////////////////////////////////////////
    


///////////////////////////////////////////////////
/// Main routines
///////////////////////////////////////////////////

void printResults(DataMin1D data)
{
    printf("Minimum: %+.16f\n", data.result);
    printf("Point:   %+2.16f\n", data.point);
    printf("Calls:   %d\n", data.nCalls);
}

int main()
{
    MyFunction f;
    DataMin1D data(1e-2, 5);
//    RealClosedRange range(-8, -4);
    RealClosedRange range(0, 3*pi);
    
    EAMin1D eaMin1D;    
    if (!eaMin1D(f, range, data)) {
        cout << "Couldn't find the minimum." << endl;
    }
    else {
        cout << "Method #1" << endl;
        printResults(data);
    }
    
    GSMin1D gsMin1D;
    if (!gsMin1D(f, range, data)) {
        cout << "Couldn't find the minimum." << endl;
    }
    else {
        cout << "Method #2" << endl;
        printResults(data);
    }
    
    HDMin1D hdMin1D;
    if (!hdMin1D(f, range, data)) {
        cout << "Couldn't find the minimum." << endl;
    }
    else {
        cout << "Method #3" << endl;
        printResults(data);
    }
    
    return 0;
}































#if 0

#include <OptMethods>
#include <limits>

using namespace std;

///////////////////////////////////////////////////
/// Generic stuff
///////////////////////////////////////////////////

/// Abstract function class
template <typename T>
class F
{
public:
    virtual T operator()(T x) {}
};
    
/// Real-value function class
typedef F<real> RealF;

/// Range struct
template <typename T>
struct GenericRange
{
	/// @invariant  Should be "b >= a". Someone can put assertions of this.
	T a, b;
	GenericRange(const T &newA, const T &newB) { a = newA; b = newB; }
	T length() { return b - a; }
};

/// Closed range struct
template <typename T>
struct ClosedRange : public GenericRange<T>
{
	ClosedRange(const T &newA, const T &newB)
	{
		this->a = newA;
		this->b = newB;
		if (numeric_limits<T>::has_infinity)
        {
			runtimeCheck(this->a != numeric_limits<T>::infinity() && this->a != -numeric_limits<T>::infinity(), 
						 "Infinity isn\'t allowed to enclose ClosedRange");
			runtimeCheck(this->b != numeric_limits<T>::infinity() && this->b != -numeric_limits<T>::infinity(), 
						 "Infinity isn\'t allowed to enclose ClosedRange");
		}
	}
};
	
/// Real-value closed range struct
typedef ClosedRange<real> RealClosedRange;

struct DataMin1D
{
    DataMin1D(real eps) : epsilon(eps) {}
    real epsilon;
    int param;
    real result;
    real point;
    int nCalls;
};

/// One dimentional function mimimization class    
template <typename T>
class Min1D
{
public:
    virtual bool operator()(F<T> f, GenericRange<T> r, DataMin1D& data) = 0;
};
/// Real variable one dimentional function mimimization class    
typedef Min1D<real> RealMin1D;


/// First method: Elena Aleksandrovna search
class EAMin1D : public RealMin1D
{
public:
    virtual bool operator()(RealF f, RealClosedRange r, DataMin1D& data)
    {
        runtimeCheck(r.a <= r.b, "Invalid range");
        
        nSteps = data.param;
        
        real fa = f(r.a);
        data.nCalls = 1;

        while (r.b - r.a > data.epsilon)
        {
            real step = (r.b - r.a) / nSteps;
            real fx = fa;
            real tmp = fa;
            real min = fa;
            int i = 1;
            for (; i != nSteps; ++i)
            {
                fa  = fx;
                fx  = tmp;
                tmp = f(r.a + i * step);
                data.nCalls++;
                if (tmp < min)
                {
                    min = tmp;
                }
                else if (tmp > min)
                {
                    break;
                }
            }
            if (i == nSteps)
            {
                r.a  = r.b - step;
                fa = fx;
            }
            else
            {
                r.b = r.a + i * step;
                if (i != 1)
                {
                    r.a = r.b - 2 * step;
                }
            }
        }
        data.point = (r.a + r.b) / 2;
        data.result = f(data.point);
    }
private:
    int nSteps;
};

/// Second method: Golden section search
class GSMin1D : public RealMin1D
{
};

    
///////////////////////////////////////////////////
/// Problem specific stuff
///////////////////////////////////////////////////
    
class MyFunction : public RealF
{    
    real operator()(real x)
    {
        return sin(x);
    }
};

///////////////////////////////////////////////////
/// Main
///////////////////////////////////////////////////

int main()
{
    MyFunction f;
    DataMin1D data(1e-3);
    EAMin1D eaMin1D;
    data = eaMin1D(f, RealClosedRange());
}




#endif
