#-------------------------------------------------
#
# Project created by QtCreator 2012-02-23T00:26:06
#
#-------------------------------------------------

QT       -= core gui

TARGET = OptMethods
TEMPLATE = lib

DEFINES += OPTMETHODS_LIBRARY

SOURCES += optmethods.cpp \
    fpcomp.cpp

HEADERS += optmethods.h\
    fpcomp.h \
    OptMethods \
    matrix.h

symbian {
    MMP_RULES += EXPORTUNFROZEN
    TARGET.UID3 = 0xE8CEA935
    TARGET.CAPABILITY = 
    TARGET.EPOCALLOWDLLDATA = 1
    addFiles.sources = OptMethods.dll
    addFiles.path = !:/sys/bin
    DEPLOYMENT += addFiles
}

unix:!symbian {
    maemo5 {
        target.path = /opt/usr/lib
    } else {
        target.path = /usr/lib
    }
    INSTALLS += target
}
