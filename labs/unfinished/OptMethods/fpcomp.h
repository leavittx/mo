#pragma once

#include <cmath>
#include <cfloat>

//const double eps = 1e-10;
const int eps = DBL_EPSILON;

template<class T>
    bool eq_zero(T val)
{
    return fabs(val) < eps;
}
	
template<class T>
	bool eq(T val1, T val2)
{
	return fabs(val1 - val2) < eps;
}

template<class T>
    bool ge(T val1, T val2)
{
    return val1 >= val2 - eps;
}

template<class T>
    bool le(T val1, T val2)
{
    return val1 <= val2 + eps;
}
