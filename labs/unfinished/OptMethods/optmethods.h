#pragma once

#ifndef OPTMETHODS_INCLUDED
#error *** Please include <OptMethods> instead of this file ***
#endif

#include <stdexcept>

/// @todo: bad design?
static void runtimeCheck(bool cond, const char* message)
{
	if (!cond) {
		throw std::runtime_error(message);
	}
}

static void logicCheck(bool cond, const char* message)
{
	if (!cond) {
		throw std::logic_error(message);
	}
}

#include <iostream>
#include <limits>
#include <cstdlib>

/// Floating-point comparison routines
#include "fpcomp.h"

/// Matrices & related routines
#include "matrix.h"

typedef double real;

const real pi = 4.f * atan(1.f);
//const real goldenRatio = (sqrt(5.0) + 1.0) / 2.0;
const real goldenRatio = (3.f - sqrt(5.f)) / 2.f;
//const real onePerGoldenRatio = goldenRatio - 1;
	

/// @todo: move to some separate header
template<class T>
    void swap(T & val1, T & val2)
{
    T t = val1;
    val1 = val2;
    val2 = t;
}

	
//#include "OptMethods_global.h"
//class OPTMETHODSSHARED_EXPORT OptMethods {
//public:
//	OptMethods();
//};
